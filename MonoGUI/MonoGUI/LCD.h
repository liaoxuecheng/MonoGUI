// LCD.h: interface for the LCD class.
// 作者：司徒汇编民间科学工作室，共和国山东青岛
//////////////////////////////////////////////////////////////////////
#include "OCommon.h"
#if !defined(__LCD_H__)
#define __LCD_H__


// 定义绘制模式
enum {
	LCD_MODE_NORMAL     = 0,
	LCD_MODE_INVERSE    = 1,
	LCD_MODE_OR         = 2,
	LCD_MODE_AND        = 3,
	LCD_MODE_NOR        = 4,
	LCD_MODE_BLACKNESS  = 5,
	LCD_MODE_WHITENESS  = 6
};

// 定义填充模式
enum {
	COPY_MODE_NORMAL    = 0,
	COPY_MODE_INVERSE   = 1,
	COPY_MODE_BLACKNESS = 2,
	COPY_MODE_WHITENESS = 3
};

// For ARCA-II Fcr Devices
#define FBIOREFRESH    0x4682           /* force to refresh framebuffer    */
#define FBIOSTOPTIMER  0x4680           /* stop auto-refresh timer         */
#define FBIOSTARTTIMER 0x4681           /* start auto-refresh timer        */

#define  ASC_W	5                       /* width of an ASCI character      */
#define  ASC_H  12                      /* height of an ASCI character     */
#define  HZK_W  11                      /* width of a CHINESE character    */
#define  HZK_H  12                      /* height of a CHINESE character   */

#define  DRAW_TEXT_LENGTH_MAX 256       /* the max length of a string on screen */

#define  ASC_GAP 1                      /* gap betweent two asc character       */
#define  HZK_GAP 1                      /* gap betweent two hzk character       */

const BYTE mask_set_bit[8]	= {0x80,0x40,0x20,0x10,0x08,0x04,0x02,0x01};
const BYTE mask_clr_bit[8]	= {0x7F,0xBF,0xDF,0xEF,0xF7,0xFB,0xFD,0xFE};
const BYTE mask_byte[16]	= {0x00,0x80,0xC0,0xE0,0xF0,0xF8,0xFC,0xFE,0xFF,0x7F,0x3F,0x1F,0x0F,0x07,0x03,0x01};

class LCD  
{
private:

	int fb_w;							/* width of FrameBuffer  */
	int fb_h;							/* height of FrameBuffer */
	BYTE* fbmem;						/* FrameBuffer 			 */
	BOOL  IsCited;                      /* point to another one? */
	
public:
	static BYTE* asc12; // [ LENGTH_OF_ASC + 256 ] = {};	/* ENGLISH FONT	*/

#if defined (CHINESE_SUPPORT)
	static BYTE* hzk12; // [ LENGTH_OF_HZK + 256 ] = {};	/* CHINESE FONT	*/
#endif // defined(CHINESE_SUPPORT)

	LCD ();
	LCD (int w, int h);
	LCD (LCD& other, BOOL bCopy = TRUE);
	LCD (BW_IMAGE& bwimg, BOOL bCopy = TRUE);
	LCD (BYTE* buffer, int w, int h, BOOL bCopy = TRUE);

	virtual ~LCD ();
	void Inverse (void);//黑白翻转
	void ReleaseBuffer (void);

#if defined (RUN_ENVIRONMENT_LINUX)
	int m_nFB;							/* framebuffer fd */
	/* !!! NOTICE: Linux may support multi-frambuffer !!! */
	/*     SO, do NOT define m_nFB as static.             */
	BOOL LinuxInit (char* dev_file = "/dev/fb0");
	void LinuxFini (void);
	void Show (void);
#endif // defined(RUN_ENVIRONMENT_LINUX)
#if defined (RUN_ENVIRONMENT_WIN32)

	BOOL Win32Init (int w, int h);
	void Win32Fini (void);
	void DebugSaveSnapshoot(char* bmp_filename);

	// 刷新显示，与操作系统有关
	void Show (HWND hWnd, BOOL bReverseMode);

#endif // defined(RUN_ENVIRONMENT_WIN32)

	BYTE* GetBuffer (int* pnW, int* pnH);
	void  CopyBuffer (BYTE* buffer, int w, int h);
	void  SetPixel (int x, int y, int color);
	int   GetPixel (int x, int y);
	void  VLine (int x, int y, int l, int color);
	void  HLine (int x, int y, int l, int color);
	void  FillRect (int x, int y, int w, int h, int color);
	void  DrawImage (int x, int y, int w, int h, FOUR_COLOR_IMAGE& img, int sx, int sy, int mode = LCD_MODE_NORMAL);
	void  DrawImage (int x, int y, int w, int h, BW_IMAGE& bwimg, int sx, int sy, int mode = LCD_MODE_NORMAL);
	void  TextOut (int x, int y, BYTE* text, int l, int mode = LCD_MODE_NORMAL);
	void  TextOut_Align (int x, int y, BYTE* text, int l, int mode = LCD_MODE_NORMAL);  // 用于英文汉字对齐模式
	void  Copy (LCD& other, int mode = COPY_MODE_NORMAL);
	void  BitBlt (int x, int y, int w, int h, LCD& other, int sx, int sy, int mode = LCD_MODE_NORMAL);
	void  InverseBlt (int x, int y, int w, int h, LCD& other, int sx, int sy, int mode = LCD_MODE_NORMAL);
	void  Line (int x1, int y1, int x2, int y2, int color);
};

#endif // !defined(__LCD_H__)
