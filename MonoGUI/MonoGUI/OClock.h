// OClock.h
// 作者：司徒汇编民间科学工作室，共和国山东青岛
//////////////////////////////////////////////////////////////////////
#if !defined(__OCLOCK_H__)
#define __OCLOCK_H__


class OClock
{
private:
	OApp* m_pApp;

	// 状态：1: 显示 0:不显示
	int  m_nStatus;
	char m_sTime[128];
	char m_sDate[128];
	LCD* m_pBuffer;

	ULONGLONG m_lLastTime;

	int m_nClockX;
	int m_nClockY;

#if defined (MOUSE_SUPPORT)
	int  m_nClockButtonX; // 时钟按钮的X
	int  m_nClockButtonY; // 时钟按钮的Y
#endif // defined(MOUSE_SUPPORT)

public:
	OClock (OApp* pApp);
	virtual ~OClock();

public:
	void Enable (BOOL bEnable);

	BOOL IsEnable();

	BOOL Check (LCD* pLCD, LCD* pBuf);

#if defined (MOUSE_SUPPORT)
	// 鼠标点击时钟按钮的处理
	BOOL PtProc (int x, int y);

	// 显示时钟按钮
	void ShowClockButton (LCD* pLCD);

	// 设置表盘的显示位置
	BOOL SetClockPos (int x, int y);

	// 设置时钟按钮的位置
	BOOL SetClockButton (int x, int y);

#endif // defined(MOUSE_SUPPORT)
};

#endif // !defined(__OCLOCK_H__)
