# MonoGUI-A Black and White Graphical User Interface System

#### Description
MonoGUI is a dedicated GUI system developed for small electronic devices with
black and white screens such as electronic dictionaries, advanced calculators,
electronic watches, label printers, cash registers, and electronic tags. The
system has the characteristics of simple structure, easy use, small memory
occupation, single thread, and light processor load. Although the graphics
capability is only black and white, it supports the complete Chinese display
processing function (2-byte Chinese characters in GB18030, that is, the old
GB13000 standard), and its Edit control and Chinese input method (including
the nine-key input method) function to achieve Windows. And Android's input
 method level, and the extension is easy.

MonoGUI is written in pure C++ and does not depend on third-party source code
for portability and debugging. The supporting tool software, such as picture
converter, dialog template editor, etc., are all open source. The design infor
-mation is complete, the documentation is substantial, and it is also a rare
research material.

#### Software Architecture
MonoGUI's software architecture design is very simple and clear, please refer
to the pdf file "MonoGUI design document".

#### Installation
The software does not need to be installed, run the "compiled DEMO program.exe"
file directly in the MonoGUI directory to view the effect. 

At runtime, the anti-virus software may prompt "discover Trojans", there is no
way, anti-virus software is always alert to unknowns. You can also run in a 
virtual machine environment.

#### Instructions
This software can be opened directly using VS2012. In fact, MonoGUI does not
rely on any third-party libraries, and is designed for cross-system porting.
The code basically does not rely on the high-version C++.

When compiling, please compile and generate MonoGUI.lib, then compile and
generate DEMO_Win32.exe file.

#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
